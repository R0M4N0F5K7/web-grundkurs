let money = 100;
document.getElementById("money").innerHTML = money;

function gamble() {

  let wager = parseInt(document.getElementById("wager").value);
  //ERROR-HANDLING - Bedingungen unter denen der Spieler keine Runde starten darf
  if (money == 0){
    alert("Sie haben kein Geld mehr zum verspielen... Besorgen Sie sich neues!");
  }else if(wager > money){
    alert("Sie können nicht mehr Geld setzen als sie haben...")
  // stellt sicher, dass eine ZAHL und mehr als 0 gesetzt wird - ohne könnte der Spieler money mit NaN überschreiben und ewig spielen
  }else if(isNaN(wager) || wager == 0){
    alert("Setzen Sie einen Betrag um zu Spielen.")

  //SPIEL-START (wenn keine Fehler-Bedingungen erfüllt wurden)
  }else{
    let bet = document.getElementById("colorRed").checked ? "Rot" : "Schwarz";
    let color = "";
    let thisRound = Math.random()*100; // *100 um besser in Prozent denken zu können
    let result;

    //Festlegen welcher Farbe der Random-Wert in thisRound entspricht
    if(thisRound < 48){
      color = "Rot";
    }else if(thisRound < 96){
      color = "Schwarz";
    }else{
      color = "Grün";
    }

    result = color == bet ? "GEWONNEN!" : "VERLOREN...";
    money = result == "GEWONNEN!" ?  money+wager : money-wager;

    document.getElementById("money").innerHTML = money;
    document.getElementById("result").innerHTML = color + "<br>Sie haben " + result;

    if (money == 0){
      alert("GAME OVER - Sie haben kein Geld mehr zum verspielen...");
    }
  }
}
