//globale Variable für Aufgabe 1
let count = 0;

//globale Variable für Aufgabe 3
let text = "";

//Aufgabe 1
function counter() {
  count++;
  document.getElementById("output").innerHTML = count;
}

//Aufgabe 2
/* Die einzelnen Funktionen sind durch die function cal() von Bonus nicht mehr notwendig

function add(){
  result = parseInt(document.getElementById("numberField1").value) + parseInt(document.getElementById("numberField2").value);
  document.getElementById("result").innerHTML = result;
}

function sub(){
  result = parseInt(document.getElementById("numberField1").value) - parseInt(document.getElementById("numberField2").value);
  document.getElementById("result").innerHTML = result;
}

function mul(){
  result = parseInt(document.getElementById("numberField1").value) * parseInt(document.getElementById("numberField2").value);
  document.getElementById("result").innerHTML = result;
}

function div(){
  result = parseInt(document.getElementById("numberField1").value) / parseInt(document.getElementById("numberField2").value);
  document.getElementById("result").innerHTML = result;
}

*/

function cal(type) {
  number1 = parseInt(document.getElementById("numberField1").value);
  number2 = parseInt(document.getElementById("numberField2").value);
  switch (type) {
    case '+':
      result = number1 + number2;
      document.getElementById("result").innerHTML = result;
      break;
    case '-':
      result = number1 - number2;
      document.getElementById("result").innerHTML = result;
      break;
    case '*':
      result = number1 * number2;
      document.getElementById("result").innerHTML = result;
      break;
    case '/':
      result = number1 / number2;
      document.getElementById("result").innerHTML = result;
      break;
  }
}

//Aufgabe 3
function textAdd() {
  text += document.getElementById("textInput").value;
  document.getElementById("text").innerHTML = text;
  document.getElementById("textInput").value = "";
}

function deleteText() {
  text = "";
  document.getElementById("text").innerHTML = text;
}

function deleteLast() {
  text = text.substr(0, text.length - 1)
  document.getElementById("text").innerHTML = text;
}

//Bonus Aufgabe
function mixColors() {
  let red = document.getElementById("red").checked;
  let green = document.getElementById("green").checked;
  let blue = document.getElementById("blue").checked;

  switch (true) {
    case red && green && blue:
      document.getElementById("mixedColor").style.backgroundColor = "white";
      break; 
    case red && green:
      document.getElementById("mixedColor").style.backgroundColor = "yellow";
      break;
    case red && blue:
      document.getElementById("mixedColor").style.backgroundColor = "magenta";
      break;
    case red:
      document.getElementById("mixedColor").style.backgroundColor = "red";
      break;
    case green && blue:
      document.getElementById("mixedColor").style.backgroundColor = "cyan";
      break;
    case green:
      document.getElementById("mixedColor").style.backgroundColor = "green";
      break;
    case blue:
      document.getElementById("mixedColor").style.backgroundColor = "blue";
      break;
    default:
      document.getElementById("mixedColor").style.backgroundColor = "black";
  }
}