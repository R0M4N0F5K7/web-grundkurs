//Klasse Person
class Person {

  constructor(firstName, lastName, birthYear) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthYear = birthYear;
  }

  getAge() {
    return new Date().getFullYear() - this.birthYear;
  }

  //getter und setter:
  get getFirstName() {
    return this.firstName;
  }

  set setFirstName(firstName) {
    this.firstName = firstName;
    updateTable();
  }

  get getLastName() {
    return this.lastName;
  }

  set setLastName(lastName) {
    this.lastName = lastName;
    updateTable();
  }

  get getBirthYear() {
    return this.birthYear;
  }

  set setBirthYear(birthYear) {
    this.birthYear = birthYear;
    updateTable();
  }

}

//Array Personen und Methoden

let personen = [];

function addPerson() {
  let firstName = document.getElementById("firstName").value; 
  let lastName = document.getElementById("lastName").value;
  let birthYear = document.getElementById("birthYear").value;

  //Verlangen, dass man bei Vorname und Nachname etwas reinschreibt (Beim vergleichen von Strings in JS bedeutet 0 leerer String)
  if (firstName == 0 || lastName == 0) {
    alert("Bitte alle Felder Ausfüllen");
  }
  //Verlangen, dass Geburtsjahr eine Zahl ist. (parseInt gibt NaN aus wenn keine Zahl; mit isNaN fehler ausgeben)
  else if (isNaN(parseInt(birthYear))) {
    alert("Geburtsjahr muss eine Zahl sein");
  }
  else {
    const person = new Person(firstName, lastName, birthYear);
    personen.push(person);
    firstName = "";
    lastName = "";
    birthYear = "";
    updateTable();
  }
}

// In der Tabelle alle Daten vom Array ausgeben
function updateTable() {
  let row = "";

  for (i in personen) {
    row += '<tr><td>' + personen[i].firstName + '</td><td>' + personen[i].lastName + '</td><td>' + personen[i].getAge() + '</td></tr>';
  }

  document.getElementById("listBody").innerHTML = row;
}

function removeAll() {
  personen = [];
  updateTable();
}

function removeLast() {
  personen.pop();
  updateTable();
}

//Bonus
function sortByLastName() {
  personen.sort(function (a, b) {
    var nameA = a.lastName.toUpperCase();
    var nameB = b.lastName.toUpperCase();
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    return 0;
  })

  updateTable();
}

function sortByAge() {
  personen.sort(function (a, b) {
    return b.birthYear - a.birthYear;
  })

  updateTable();
}