let addressAvaliable = false;
let orderSent = false;
let emptyInput = document.querySelectorAll("form input").length;
//für Zugriff auf aktuelle Items im Warenkorb (veränderbar - wird regelmäßig überschrieben)
let removeItemBadges = document.querySelectorAll("#shoppingCart .badge");
//für Zugriff auf bestellbare Items der Speisekarte
const addItemBadges = document.querySelectorAll("#accordionOrders .badge");
const orderButton = document.getElementById("orderButton");


//lässt bei NavBar Menu zusammenklappen wenn ein link darin gecklickt wird
  const navLinks = document.querySelectorAll('.nav-item:not(.dropdown)')
  const menuToggle = document.getElementById('navbarNav')
  const bsCollapse = new bootstrap.Collapse(menuToggle, { toggle: false })
  navLinks.forEach(function (l) {
    l.addEventListener('click', function () {
      if (menuToggle.classList.contains('show')) {
        bsCollapse.toggle();
      }
    });
  });

// Bei Bestellformular auf order.html: Werbungshinweis wenn nicht gechecked
const checkbox = document.getElementById("gridCheck");
const checkText = document.getElementById("gridCheckText");
let counter = 0;


//Adresse mit Formular übermitteln
function submitAddress(){
  document.querySelectorAll("form input").forEach((inputField) => {
    if (!inputField.value){
      inputField.style.setProperty("--placeholder-color", "red");
      inputField.setAttribute("placeholder", "BITTE AUSFÜLLEN!");
      emptyInput--;
    }
  })
  // User pushen um Werbung zu akzeptieren
  if (!checkbox.checked&&emptyInput==9){
    counter++;
  
    switch(counter){
      case 1: 
        checkText.innerHTML = "Wahrscheinlich haben Sie's übersehen. Hier klicken für tolle 'Angebote' von uns.</p>";
        checkText.style.color = "red";
        break;
      case 2:
        checkText.innerHTML = "Ernsthaft? Normalerweise funktioniert das wenn wir oft genug nerven. Komm, lass dich von uns zumüllen...";
        checkText.style.fontSize = "1.5rem";
        break;
      case 3:
        checkText.innerHTML = "BITTE, BITTE, WIR WOLLEN DOCH SO GERNE DEINE ADRESSE FÜR ALLES MÖGLICHE VERWENDEN...";
        checkText.style.fontSize = "2rem";
        break;
      default:
        checkText.innerHTML = "...nagut, dann halt keine Werbung...";
        checkText.style.color = "grey";
        checkText.style.fontSize = "1rem";
        checkbox.style.display = "none";
        addressSubmitted();
        emptyInput = document.querySelectorAll("form input").length;
    }
  }else{
    if (emptyInput==9){
      addressSubmitted();
    }
    emptyInput = document.querySelectorAll("form input").length;
  }
}

// aufrufen wenn Adresse erfolgreich übermittelt wurde (deaktiviert Formularbutton und ermöglicht abschicken der Bestellung)
function addressSubmitted(){
  const button = document.getElementById("addressButton");
  button.setAttribute("disabled", "true");
  button.innerHTML = "Adresse erfolgreich bestätigt";
  addressAvaliable = true;
  orderButton.innerHTML = "Bestellung abschicken";
  
}

////////////////////////////
//WARENKORB-FUNKTIONALITÄT//
////////////////////////////

//Warenkorb-Button nur anzeigen wenn mindestens 1 Produkt im Warenkorb
function checkIfCartEmpty() {
  if(removeItemBadges.length){
    document.getElementById("cartButton").style.display = "block";
  }else{
    document.getElementById("cartButton").style.display = "none";
  }
}

//hinzufügen von Waren in den Warenkorb
addItemBadges.forEach((badge) => {
  badge.addEventListener("click", (event) => {
    if(!orderSent){
      const product = event.target.parentElement.parentElement.getElementsByTagName("th")[0].innerText;
      const price = event.target.parentElement.parentElement.getElementsByTagName("td")[0].innerText.replace('+', '');
      let cartRow = document.createElement('tr');
      cartRow.innerHTML = `
        <tr>
          <th scope="row"><span class="mx-1">${product}</span></th>
          <td class = "d-flex justify-content-end">${price}<button class="badge bg-primary ms-3">-</button></td>
        </tr>`;
      document.getElementById("shoppingCart").getElementsByTagName("tbody")[0].append(cartRow);

      //EventListener für entfernen von Waren aus den Warenkorb für aktuelle Liste
      removeItemBadges = document.querySelectorAll("#shoppingCart .badge");
      removeItemBadges.forEach((badge) => {
        badge.addEventListener("click", (event) => {
          if(!orderSent){
            event.target.closest("tr").remove();
            removeItemBadges = document.querySelectorAll("#shoppingCart .badge");

            //offCanvas schließen wenn man letztes Item aus Warenkorb entfernt
            if(removeItemBadges.length == 0){
                document.getElementById("cartButton").click();
            }

            checkIfCartEmpty();
            updateTotal();
          }
        });
      })

      checkIfCartEmpty();
      updateTotal();
    }
  });
})

//Gesammtpreis im Warenkorb aktualisieren (nach enfernen und hinzufügen von Produkten aufrufen)
function updateTotal(){
  let total = 0;
  removeItemBadges.forEach((badge) => {
    total += parseFloat(badge.closest("td").textContent.replace('€' , '').replace('-', '').replace(',', '.'));
  })
  total = (Math.round(total * 100)/ 100).toFixed(2);
  document.getElementById('result').innerText = String(total).replace('.', ',') + '€';
}

//Bestellung abschicken - Hinweis, falls Adressformular noch nicht ausgefüllt
function submitOrder(){
  if (!addressAvaliable){
    orderButton.innerHTML = "Bitte erst Adressformular ausfüllen";
    document.getElementById("addressButton").scrollIntoView();
  }else{
    orderButton.setAttribute("disabled", "true");
    orderButton.innerHTML = "Danke für Ihre Bestellung";
    orderSuccessful();
  }
}

// wenn bestellt: Danke und voraussichtliche Bestellzeit anzeigen
function orderSuccessful(){
  orderSent = true;
  let date = new Date();
  let orderNotification = document.createElement('div');
  orderNotification.innerHTML = "<br>Bestellung getätigt um: " + date.getHours() + ":" + date.getMinutes()+"<br><br>Voraussichtliche Wartezeit: 30 Minuten<br><br>";
  orderNotification.classList.add("m-4");
  orderNotification.style.backgroundColor = "darkred";
  orderNotification.style.borderRadius = "25px";
  orderNotification.style.textAlign = "center";
  document.getElementById("shoppingCart").append(orderNotification);
}
