### Unternehmens-Homepage mit Online-Lieferservice / Bestellfunktion / Responsive Design

![Restaurant Logo](/images/logo.png "Luigi's Mansion")

Dieses Projekt beinhaltet eine Homepage welche als Wochenendprojekt erstellt wurde, um grundlegende Fähigkeiten beim arbeiten ohne größerer Frameworks zu demonstrieren.

- Mit HTML, CSS, Vanilla JS und Bootstrap wurde eine Homepage zur Online Präsenz eines Restaurants umgesetzt.

- Im weiteren wurde eine Lieferservice-Seite hinzugefügt und eine Bestell- und Warenkorbfunktion mit Vanilla JS implementiert.

- Auf detailgetreues Responive Design - für angepasste Anzeige auf kleinsten Handy-Bildschirmen, über Tablets und Laptops, bis zu großen hochauflösenden Desktop-Displays wurde wert gelegt.

<br>

![Desktop Ansicht der Homepage](/images/desktop1.png "Desktopansicht")
Desktop Ansicht

<br>

![Laptop Ansicht der Homepage](/images/laptop.png "Laptopansicht")
Laptop Ansicht

<br>

![Mobile Ansicht der Homepage](/images/mobile.png "Mobileansicht")
Mobile Ansicht

<br>


----

<br>

_Im Ordner "JavaScript Exercises" befinden sich weitere kleine Spielereien, welche grundlegende Logik und Kenntnis zur Umsetzung von Javascript-Aufgabenstellungen demonstrieren_
